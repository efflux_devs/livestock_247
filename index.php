<?php
/**
 * Created by PhpStorm.
 * User: ls247-02
 * Date: 2/12/19
 * Time: 9:06 AM
 */
include 'header.php';
include 'slider.php';
?>
<title>Livestock247 :: Home</title>
 <!-- Page Content -->
<div class="container">
  	<!-- whitespace --->
  	<div class="space"></div>

<!-- Portfolio Section -->
  
    <div class="row">
        <div class="col-md-6  sell-content">
          	<div class="sell-1"> Sell your Livestock to <br>Potential Buyers on <br>our Platform at a glance</div>
            <div class="sell-2">
                Livestock producer, merchant or rancher’s sell his <br> product and get good value
                without stress and<br>any in exception of the middlemen
                bugs.
            </div>
		    <div class="sell-2">
                <p>Are you a livestock merchant, trader, or producer?</p>
                <p>Are you a rancher?</p>
                <span>Do you need a platform to sell your livestock that works<br>24/7 all day, everyday?</span>
            </div>
             <a href="#" class="btn btn-green" style="margin-left: 25px; margin-top:25px;">Learn More</a>
        </div>

        <div class="col-md-6 brighten ">
            <img src="images/cow-1.png"  alt="Sell your livestock" class="sell-3">
        </div>
    </div> <!-- row -->
</div>
<!-- /.row -->

<div class="space"></div>
  <!-- container -->
<div class="blue-banner">
    <div class="container">
        <div class="row">
            <div  class= "col-md-6">
                <div class="easy-text">
                  Easily Track Livestock<br>
                      With Their Chip Number
                </div>
                <div class="sell-2">Get to know the status of your order either by location or condition<br>
                  and also the specific amount of time it’ll take the parcel to get to<br>your current location
                </div>
            </div>

            <div class="col-md-6">
                <div class="track-text">Track your livestock </div>
                    <form class="form-inline">
                        <div class="form-group track-page">
                          <input type="text" class="form-control track-field"  placeholder=" Input your chip number here">
                          <i class="fa fa-search btn btn-search mb-2" aria-hidden="true"></i>
                        </div>
                    </form>
                </div>
             </div>
        </div><!-- container -->
    <div class="space"></div>
</div><!-- banner -->

<div class="space"></div>
<div class="container">
    <div class="row">
        <div  class="col-md-6">
            <div class="discover"></div>
            <div class="vet-1">
                <img src="images/vet-1.jpg">
            </div>
        </div>

        <div class="col-sm-6">
            <div class="discover-us">
                <p class="vet-heading">About Us</p>
                <p class="vet-text">Livestock247.com is an online livestock market and listing platform.
                    We are based in Africa, Nigeria.</p>

                <p class="vet-text">We work together to  create and produce good food that we are proud of for people and organizations.
                    <br>we believe in bringing together;</p>

                <div class="row">
                    <div class="col-sm-3 tilt ">
                        <img src="images/icon-1.png" width="64" height="69" alt="Buyers">Buyers
                    </div>

                    <div class="col-sm-3 tilt ">
                    <img src="images/icon-2.png" width="63" height="69" alt="Sellers">Sellers
                    </div>

                    <div class="col-sm-3 tilt ">
                    <img src="images/icon-3.png" width="63" height="69" alt="Ranchers">Ranchers
                    </div>

                    <div class="col-sm-3 tilt ">
                    <img src="images/icon-4.png" width="63" height="69" alt="Livestock Merchants and Traders"> Livestock Merchants & Traders
                    </div>
                </div><!-- icon-row -->

                <div class="row">
                    <div class="col-sm-3 tilt ">
                        <img src="images/icon-5.png" width="64" height="69" alt="Veterinary professionals">Veterinary professionals
                    </div>

                    <div class="col-sm-3 tilt">
                        <img src="images/icon-6.png" width="63" height="69" alt="Haulage and Logistics Companies">Haulage & Logistics Companies
                    </div>

                    <div class="col-sm-3 tilt">
                        <img src="images/icon-7.png" width="63" height="69" alt="Financial Service Providers">Financial Service Providers
                    </div>
                    <a href="#" class="btn btn-green02" style="margin-left: 20px; margin-top:25px;">Discover More</a>
                </div><!-- icon-row -->
            </div> <!-- discover-us  --->
        </div>
    </div>
</div><!-- container -->

<div class="space"></div>

<div class="container">
    <div class="space"></div>

    <div class="be-partner">
        <p class="vet-heading">Be A Partner <br></p>
    <div class="row">
        <div  class= "col-md-6 brighten">
        <img src="images/vet-2.jpg" alt="Be a Partner">
            <p></p>
            <p class="partner-sub">Be an Agent</p>
            <p class="vet-text">A Livestock247.com agent MUST be a qualified veterinary professional certified by
                the veterinary council of Nigeria (VCN) or the Nigeria institute of animal science (NIAS)</p>
            <a href="#" class="btn btn-green02">Discover More</a>
        </div>

        <div  class= "col-md-6 brighten">
            <img src="images/vet-3.jpg" alt="Be a Partner">
            <p></p>
            <p class="partner-sub">Butchery/Abattoir</p>
            <p class="vet-text">A Livestock247.com agent MUST be a qualified veterinary professional certified by the
                veterinary council of Nigeria (VCN) or the Nigeria institute of animal science (NIAS)</p>
            <a href="#" class="btn btn-green02">Discover More</a>
        </div>
    </div></div>

    <div class="space"></div>
</div><!-- container -->


<!-- Video player  --->
<div class="overlay"></div>
    <div  class="video" >
        <iframe src="https://www.youtube.com/embed/ii3dqHyeiVo?autoplay=1&mute=1&loop=1;" frameborder="0" allowfullscreen controls="0">
        </iframe>
    </div>

  <!-- Video player  --->

<div class="space"></div>

<!-- Our Blog  --->
<div class="container">
    <p class="vet-heading">Our Blog<br></p>
    <div class="row">
        <p></p>
        <div class="col-md-3 blog">
            <img src="images/vet-5.jpg">
                <div class="post blog-1">

                    <div  class="vet-text" style="padding:12px 0 12px 0;"> 01, March 2019</div>
                    <h6 style="text-align: left"><a href="#">ONLINE PLATFORM TO BRIDGE IN LIVESTOCK.....</a></h6>
                    <div class="vet-text">There is a momonet in the life of any aspiring...</div>

            </div>
        </div>

        <div class="col-md-3 blog">
            <img src="images/vet-5.jpg">
            <div class="post blog-2">

                <div  class="vet-text" style="padding:12px 0 12px 0;"> 01, March 2019</div>
                <h6 style="text-align: left"><a href="#">ONLINE PLATFORM TO BRIDGE IN LIVESTOCK.....</a></h6>
                <div class="vet-text">There is a momonet in the life of any aspiring...</div>

            </div>
        </div>

        <div class="col-md-3 blog">
            <img src="images/vet-5.jpg">
            <div class="post blog-1">

                <div  class="vet-text" style="padding:12px 0 12px 0;"> 01, March 2019</div>
                <h6 style="text-align: left"><a href="#">ONLINE PLATFORM TO BRIDGE IN LIVESTOCK.....</a></h6>
                <div class="vet-text">There is a momonet in the life of any aspiring...</div>

            </div>
        </div>

        <div class="col-md-3 blog">
            <img src="images/vet-5.jpg">
            <div class="post blog-2">

                <div  class="vet-text" style="padding:12px 0 12px 0;"> 01, March 2019</div>
                <h6 style="text-align: left"><a href="#">ONLINE PLATFORM TO BRIDGE IN LIVESTOCK.....</a></h6>
                <div class="vet-text">There is a momonet in the life of any aspiring...</div>

            </div>
        </div>
    </div> <!-- row  --->

    <div class="space"></div>
</div> <!-- container Our Blog  --->

<!-- Our Partner --->
<div class="container">
    <p class="vet-heading">Our Partners<br></p>
    <div class="customer-logos">
        <div class="slide"><img src="images/partners/sponge.png"></div>
        <div class="slide"><img src="images/partners/sponge.png"></div>
        <div class="slide"><img src="images/partners/sponge.png"></div>
        <div class="slide"><img src="images/partners/sponge.png"></div>
        <div class="slide"><img src="images/partners/sponge.png"></div>
    </div>
    <div class="space"></div>
</div>


<div class="container">
    <p class="vet-heading"> Our Happy Client <br></p>
    <div class="customer-logos">
        <div class="slide">
            <div class="blog-3">
                <div class="post">
                   <p class="vet-text"> my cow arrived sooner than i expected. keep the good work</p>
                    <p> Dennis Tunde</p>
                </div>
            </div>
        </div>

        <div class="slide">
            <div class="blog-3">
                <div class="post">
                   <p class="vet-text"> my cow arrived sooner than i expected. keep the good work</p>
                    <p> Dennis Tunde</p>
                </div>
            </div>
        </div>

        <div class="slide">
            <div class="blog-3">
                <div class="post">
                   <p class="vet-text"> my cow arrived sooner than i expected. keep the good work</p>
                    <p> Dennis Tunde</p>
                </div>
            </div>
        </div>

        <div class="slide">
            <div class="blog-3">
                <div class="post">
                   <p class="vet-text"> my cow arrived sooner than i expected. keep the good work</p>
                    <p> Dennis Tunde</p>
                </div>
            </div>
        </div>
        
        <div class="slide">
            <div class="blog-3">
                <div class="post">
                   <p class="vet-text"> my cow arrived sooner than i expected. keep the good work</p>
                    <p> Dennis Tunde</p>
                </div>
            </div>
        </div>
    </div>
    <div class="space"></div>
</div>       
    

<!-- clients --->

<div class="space"></div>
<?php
include 'footer.php';
?>


