<?php
/**
 * Created by PhpStorm.
 * User: ls247-02
 * Date: 2/12/19
 * Time: 9:06 AM
 */
include 'header.php';
?>
    <title> Blog :: Livestock247 </title>

		<div class="container" style="margin-top: 135px;">
			<div class="row">
				<div class="col-md-6">
					<div class="blog-head" style="margin-top: 20px;">
						<h1>Blog</h1>
						<p>
							The latest and best articles selected <br> by our editorial choice 
						</p>
					</div>
				</div>

				<div class="col-md-6">
					<div class="second-col-div" >
	                    <h1>Choose by category</h1>
	                    <select class="selectpicker">
						  <option>World</option>
						  <option>Agriculture</option>
						  <option>Culture</option>
						  <option>Technology</option>
						  <option>Business</option>
						</select>
	                </div>
				</div>
			</div>
		</div>

		<div class="container">
			<div class="row">
				<div class="col-md-6">
					<div class="column-content">
						<div class="card bg-dark text-white">
							<img class="card-img" src="images/tech.png" alt="Card image">
							<div class="card-img-overlay">
							    <h5 class="card-title">Technology</h5>
							    <p class="card-text">It's a broader card with text below as a natural lead-in to extra content. This content is a little longer.</p>
							    <p class="card-text">Last updated 3 mins ago</p>
							</div>
						</div>
						 <br> 
						<div class="row">
							<div class="col-sm-6">
							    <div class="card">
							    	<img src="images/sport.png" alt="placeholder+image" style="object-fit: cover;">
							      	<div class="card-img-overlay">
							      		<h5 class="title-h5">sport</h5>
							        	<p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicin.</p>
							      	</div>
							    </div>
							</div>

							<div class="col-md-6">
							    <div class="card">
							    	<img src="images/vet_1.png" alt="placeholder+image" style="object-fit: cover;">
							      	<div class="card-img-overlay">
							      		<h5 class="title-h5">sport</h5>
							        	<p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicin.</p>
							      	</div>
							    </div>
							</div>
						</div>
					</div>
						<br>
					<div class="column-content">
						<div class="card bg-dark text-white">
							<img class="card-img" src="images/tech.png" alt="Card image">
							<div class="card-img-overlay">
							    <h5 class="card-title">Business</h5>
							    <p class="card-text">It's a broader card with text below as a natural lead-in to extra content. This content is a little longer.</p>
							</div>
						</div>
						 <br> 
						<div class="row">
							<div class="col-sm-6">
							    <div class="card">
							      	<div class="card bg-primary">
									    <img src="images/vet_1.png" alt="placeholder+image" style="object-fit: cover;">
								      	<div class="card-img-overlay">
								      		<h5 class="title-h5">Agriculture</h5>
								        	<p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicin.</p>
								      	</div>
							    	</div>
							    </div>
							</div>
							<div class="col-sm-6">
							    <div class="card">
							      <div class="card bg-primary">
								    <img src="images/sport.png" alt="placeholder+image" style="object-fit: cover;">
							      	<div class="card-img-overlay">
							      		<h5 class="title-h5">sport</h5>
							        	<p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicin.</p>
							      	</div>
							    </div>
							    </div>
							</div>
						</div>
					</div>
				</div>

				<div class="col-md-6">
					<div class="column-content">
						<div class="row">
							<div class="col-md-6">
							    <div class="card bg-primary">
							    <img src="images/vet_1.png" alt="placeholder+image" style="object-fit: cover;">
						      	<div class="card-img-overlay">
						      		<h5 class="title-h5">Agriculture</h5>
						        	<p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicin.</p>
						      	</div>
						    </div>
							</div>
							<div class="col-md-6">
							    <div class="card">
								    <div class="card">
									    <img src="images/sport.png" alt="placeholder+image" style="object-fit: cover;">
								      	<div class="card-img-overlay">
								      		<h5 class="title-h5">sport</h5>
								        	<p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicin.</p>
								      	</div>
							    	</div>
							    </div>
							</div>
						</div>

						 <br> 

						<div class="card bg-dark text-white">
							<img class="card-img" src="images/outdoor.png" alt="Card image">
							<div class="card-img-overlay">
							    <h5 class="card-title">Culture</h5>
							    <p class="card-text">It's a broader card with text below as a natural lead-in to extra content. This content is a little longer.</p>
							    <p class="card-text">Last updated 3 mins ago</p>
							</div>
						</div>
					</div>
					<br>
					<div class="column-content">
						<div class="card-deck">
						    <div class="card">
							    <img src="images/vet_1.png" alt="placeholder+image" style="object-fit: cover;">
						      	<div class="card-img-overlay">
						      		<h5 class="title-h5">sport</h5>
						        	<p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicin.</p>
						      	</div>
						    </div>

						    <div class="card">
						    	<img src="images/sport.png" alt="placeholder+image" style="object-fit: cover;">
						      	<div class="card-img-overlay">
						      		<h5 class="title-h5">sport</h5>
						        	<p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicin.</p>
						      	</div>
						    </div> 
					  	</div>
					</div>
				</div>
			</div>
		</div>

		<div class="space"></div>
		<?php
			include 'footer.php';
		?>