<?php
/**
 * Created by PhpStorm.
 * User: ls247-02
 * Date: 2/12/19
 * Time: 1:21 PM
 */?>

<footer>
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <div class="footer-post">
                    <p>Get our latest news</p>
                    <p style="font-size: 35px;">Newsletter</p>
                </div>
            </div>
            <div class="col-md-8">
                <form class="form-inline">
                    <div class="form-group footer-post" >
                        <input type="text" class="form-control footer-news"  placeholder=" Newsletter" >
                        <span class="btn btn-news">Subscribe <i class="fa fa-arrow-right" aria-hidden="true"></i>
                        </span>

                    </div>

                </form>
            </div>
        </div>
    </div>
    <div class="space"></div>
    <div class="space"></div>


    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <img src="images/logo.jpg" width="120" height="75" alt="Livestock247.com">
                <p class="footer-text">The vision of Livestock247.com is to mitigate the spread of
                    zoonotic diseases through the provision of fit-for-slaughter and
                    traceable livestock to our customers.</p>
            </div>
            <div class="col-md-4">
                <h6>quicklinks</h6>
                <div class="footer-sub" >
                    <p><a href="index.php">Home</a></p>
                    <p><a href="about.php">About Us</a></p>
                    <p><a href="#">Be a Partner</a></p>
                    <p ><a href="blog.php">Blog</a></p>
                </div>

            </div>
            <div class="col-md-4">
                <h6>contact us</h6>
                <div class="row">
                    <div class="col-lg-3">
                        <div class="footer-sub" >
                            <p>Phone:</p>
                            <p>Email:</p>
                            <p>Address:</p>
                        </div>
                    </div>

                    <div class="col-lg-9">
                        <div class="footer-sub" >
                            <p>0906-290-3550</p>
                            <p>support@livestock247.com</p>
                            <p>4th Floor, Valley View Plaza,
                                99 Opebi Road, Ikeja, Lagos-Nigeria.</p>
                        </div>
                    </div>
                </div>




            </div>
        </div> <!-- row  -->
    </div><!-- container --->

    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
               <a href="https://play.google.com/store/apps/details?id=com.livestock.livestock247" target="_blank"><img src="images/icon-apple.png" width="125" height="45"></a>
                <a href="https://itunes.apple.com/gb/app/livestock247/id1441361537?mt=8" target="_blank"><img src="images/icon-google.png" width="125" height="43"></a>
            </div>
        </div><!-- row --->
    </div><!-- container --->

    <div class="space"></div>

    <div class="container">
        <div class="row">
            <div class="col-md-4 copyright">
                &copy; 2019 livestock247
            </div>

            <div class="col-md-4 copyright">
                <div class="row">
                    <div class="col-sm-3"><a href="#">FAQ</a></div>
                    <div class="col-sm-3"><a href="#">PRIVACY</a></div>
                    <div class="col-sm-6"><a href="#">TERMS & CONDITION</a></div>
                </div>
            </div>

            <div class="col-md-4 copyright">
              <span> <a href=https://www.facebook.com/Livestock247ng/" target="_blank" title="Facebook"> <i class="fa fa-facebook-square" aria-hidden="true"></i></a></span>
               <span><a href="https://twitter.com/livestock247ng"  target="_blank" title="Twitter"> <i class="fa fa-twitter-square" aria-hidden="true"></i></a></span>
                 <span><a href="https://www.instagram.com/livestock247ng/" target="_blank" title="Instagram"> <i class="fa fa-instagram" aria-hidden="true"></i></a></span>
                  <span><a href="https://www.youtube.com/channel/UCYSw3HTXyWbc1rFKhlifK8Q/videos" target="_blank" title="Youtube"> <i class="fa fa-youtube-square" aria-hidden="true"></i></a></span>
            </div>

        </div><!-- row --->
        <p></p>
    </div><!-- container --->


</footer>
<!-- Bootstrap  JavaScript -->
<script src="js/jquery.min.js"></script>
<script src="css/bootstrap/js/bootstrap.min.js"></script>
<script src="js/custom.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<script src="js/slider.js"></script>

