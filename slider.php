<?php
/**
 * Created by PhpStorm.
 * User: ls247-02
 * Date: 2/12/19
 * Time: 9:06 AM
 */
?>
<header>
    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
      <ol class="carousel-indicators">
        <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
        <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
        <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
          <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
      </ol>
      <div class="carousel-inner" role="listbox">
        <!-- Slide One - Set the background image for this slide in the line below -->
        <div class="carousel-item active" style="background-image: url('slider/slide-1.jpg')">
          <div class="carousel-caption ">
              <a href="#" class="btn btn-green"  style="margin-left: 50px; ">Learn More</a>
              <a class="nav-link btn btn-white" href="#">Buy Now</a>
          </div>
        </div>
        <!-- Slide Two - Set the background image for this slide in the line below -->
        <div class="carousel-item" style="background-image: url('slider/slide-2.jpg')">
          <div class="carousel-caption">

                  <a href="#" class="btn btn-green">Learn More</a>
                  <a class="nav-link btn btn-white" href="#">Buy Now</a>

          </div>
        </div>
        <!-- Slide Three - Set the background image for this slide in the line below -->
        <div class="carousel-item" style="background-image: url('slider/slide-3-1.jpg')">
          <div class="carousel-caption ">
              <a href="#" class="btn btn-green" >Learn More</a>
              <a class="nav-link btn btn-white" href="#">Buy Now</a>
          </div>
        </div>
          <!-- Slide four - Set the background image for this slide in the line below -->
          <div class="carousel-item" style="background-image: url('slider/slide-4-1.jpg')">
              <div class="carousel-caption">
                  <a href="#" class="btn btn-green" style="margin-left: 35px;">Learn More</a>
                  <a class="nav-link btn btn-white" href="#">Buy Now</a>
              </div>
          </div>

      </div>
      <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only"></span>
      </a>
      <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only"></span>
      </a>
    </div>
  </header>