<?php
/**
 * Created by PhpStorm.
 * User: ls247-02
 * Date: 2/12/19
 * Time: 9:06 AM
 */
include 'header.php';
?>
	<title> About :: Livestock247</title>
	
	<!--  -->
	
	<div class="about-banner">
		<div class="container">
			<h1>About Us</h1>
		</div>
	</div>	
	
	<div class="box-wrapper">
		<div class="container">
			<div class="box-set">
			  	<figure class="box box-1">
			  		<div>
			  			<h1>Who We Are</h1>
			  			<p>
			  				Livestock247.com is an online livestock market and listing platform. We are based in Africa, Nigeria. <br> <br>

			  				We work together to create and produce good food that we are proud of for people and organizations. <br>

			  				We believe in bringing together;
			  			</p>

			  			<ul>
			  				<li>
			  					<img src="images/www.png" alt="placeholder+image">
			  					<span class="caption">Buyers</span>
			  				</li>
			  				<li>
			  					<img src="images/bag.svg" alt="placeholder+image">
			  					<span class="caption">sellers</span>
			  				</li>
			  				<li>
			  					<img src="images/user.svg" alt="placeholder+image">
			  					<span class="caption">Ranchers</span>
			  				</li>
			  				<li>
			  					<img src="images/group.svg" alt="placeholder+image">
			  					<span class="caption"> Merchants <br> & Traders </span>
			  				</li>
			  				<li>
			  					<img src="images/boss.svg" alt="placeholder+image">
			  					<span class="caption"> Veterinary & Professinals </span>
			  				</li>
			  				<li>
			  					<img src="images/trucking.svg" alt="placeholder+image">
			  					<span class="caption">Haulage & Logistics Company </span>
			  				</li>
			  				<li>
			  					<img src="images/debit-card.svg" alt="placeholder+image">
			  					<span class="caption">Financial Service Providers</span>
			  				</li>
			  			</ul>
			  		</div>
			  	</figure>
			  	<figure class="box box-2"></figure>
			</div>
		</div>
	</div>
	

	<div style="background: #2078BF;margin-top: 690px;height: 360px;">
		<div class="container">
			<div class="box-set">
				<div class="box-element box-set-element">
					
				</div>

				<div class="box-element box-set-element-1">
					<h1>Our History</h1>
					<p>
						It is a long established fact that a reader will be distracted by the raedable content of a page when looking at its layout. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Culpa dolorum nesciunt in quasi, nam nostrum debitis, dignissimos doloremque voluptatum inventore obcaecati repellat nobis reiciendis ipsa illum ad repellendus eveniet maiores.
					</p>
				</div>
			</div>
		</div>
	</div>
	
	<div class="team-wrapper">
		<h1>Our Team</h1>
		<div class="container">
			<div class="row">
				<div class="column">
				    <div class="card">
				      <img src="images/laptopguy.png" alt="Jane" style="width:100%">
				      <div class="container">
				        <h2>Jane Doe</h2>
				        <p class="title">CEO &amp; Founder</p>
				        <p>Some text that describes me lorem ipsum ipsum lorem.</p>
				        <p>example@example.com</p>
				        <!-- <p><button class="button">Contact</button></p> -->
				      </div>
				    </div>
				</div>

			  	<div class="column">
				    <div class="card">
				      <img src="images/laptopguy.png" alt="Mike" style="width:100%">
				      <div class="container">
				        <h2>Mike Ross</h2>
				        <p class="title">General Manager</p>
				        <p>Some text that describes me lorem ipsum ipsum lorem.</p>
				        <p>example@example.com</p>
				      </div>
				    </div>
			  	</div>

			  	<div class="column">
				    <div class="card">
				      	<img src="images/laptopguy.png" alt="John" style="width:100%">
					    <div class="container">
					        <h2>John Doe</h2>
					        <p class="title">Sales Representative</p>
					        <p>Some text that describes me lorem ipsum ipsum lorem.</p>
					        <p>example@example.com</p>
					   	</div>
				    </div>
			  	</div>

			  	<div class="column">
				    <div class="card">
				      	<img src="images/laptopguy.png" alt="John" style="width:100%">
				      	<div class="container">
					        <h2>John Doe</h2>
					        <p class="title">Human Resources</p>
					        <p>Some text that describes me lorem ipsum ipsum lorem.</p>
					        <p>example@example.com</p>
				      	</div>
				    </div>
			  	</div>
			</div> 
		</div>
	</div>
	


	<script src="js/jquery-2.2.3.min.js"></script>
	<script src="js/bootstrap.js"></script>

	<div class="space"></div>
	<?php
		include 'footer.php';
	?>
