<!DOCTYPE html>
  <html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Edit - Livestock247</title>

    <!-- Custom fonts for this template-->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link href="../fonts/ubuntu.css" rel="stylesheet" type="text/css">

    <!-- Page level plugin CSS-->
    <link href="vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="css/sb-admin.css" rel="stylesheet">
    <link href="css/index.css" rel="stylesheet">
  </head>

  <body id="page-top">
    <!-- Header -->
    <?php include("header.php"); ?>

    <div id="wrapper">
      <!-- Sidebar -->
      <?php include("sidebar.php"); ?>

      <div id="content-wrapper">
        <div class="container-fluid">
          <!-- Breadcrumbs-->
          <ol class="breadcrumb">
            <li class="breadcrumb-item">
              <h4 class="overview_summary">Edit Profile</h4>
            </li>
          </ol>

          <div class="jumbotron jumbotron_background">
            <div class="container" style="margin-bottom: 20px;">
              <div class="row">
                <div class="col-md-9">
                  <p style="color: #6c757d;">
                    <em>
                      Fields with <span style="color: red;">*</span> are required
                    </em>
                  </p>
                </div>

                <div class="col-md-3">
                  <a href="" data-toggle="modal" data-target="#exampleModal" class="edit_profile_reset_password_button">
                    Reset Password
                  </a>
                </div>
              </div>
            </div>

            <div class="container">
              <div class="row">
                <div class="col-md-12">
                  <form class="needs-validation" novalidate>
                    <div class="form-row">
                      <div class="col-md-12">
                        <label for="validationCustom01" class="edit_profile_label">Name <span style="color: red;">*</span></label>
                        <input type="text" class="form-control edit_profile_form_control" id="validationCustom01" placeholder="Olubodun Akinyele"
                          required>
                        <div class="valid-feedback">
                          Looks good!
                        </div>
                      </div>
                    </div>

                    <div class="form-row">
                      <div class="col-md-12">
                        <label for="validationCustom03" class="edit_profile_label">Phone <span style="color: red;">*</span></label>
                        <input type="text" class="form-control edit_profile_form_control" id="validationCustom03" placeholder="08068869417" required>
                        <div class="invalid-feedback">
                          Please provide a valid number.
                        </div>
                      </div>
                    </div>

                    <div class="form-row">
                      <div class="col-md-12">
                        <label for="validationCustom03" class="edit_profile_label">E-mail <span style="color: red;">*</span></label>
                        <input type="text" class="form-control edit_profile_form_control" id="validationCustom03" placeholder="akinyeleolubodun@yahoo.com" required>
                        <div class="invalid-feedback">
                          Please provide a valid email.
                        </div>
                      </div>
                    </div>

                    <div class="form-row">
                      <div class="col-md-12">
                        <div class="form-group">
                          <label for="exampleFormControlSelect1" class="edit_profile_label">Location</label>
                          <select class="form-control edit_profile_form_control_select" id="exampleFormControlSelect1">
                            <option>Lagos</option>
                            <option>Lagos</option>
                            <option>Lagos</option>
                            <option>Lagos</option>
                            <option>Lagos</option>
                          </select>
                        </div>

                        <label for="validationCustom03" class="edit_profile_label">Address</label>
                        <textarea class="form-control edit_profile_textarea" id="validationCustom03" placeholder="1 Solaru Street"></textarea>
                        <div class="invalid-feedback">
                          Please provide a valid address.
                        </div>
                      </div>
                    </div>

                    <div class="form-group">
                      <div class="form-check">
                        <input class="form-check-input" type="checkbox" value="" id="invalidCheck" required>
                        <label class="form-check-label" for="invalidCheck">
                          Agree to terms and conditions
                        </label>
                        <div class="invalid-feedback">
                          You must agree before submitting.
                        </div>
                      </div>
                    </div>
                    <div style="text-align: center;">
                      <button class="btn btn-success" type="submit">Save</button>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>

          <!-- Footer -->
          <?php include("copyright.php"); ?>
        </div>
      </div>

      <!-- Modal -->
      <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <!-- Breadcrumbs-->
              <ol class="breadcrumb modal_breadcrumb">
                <li class="breadcrumb-item">
                  <h4 class="overview_summary">Change Password</h4>
                </li>
              </ol>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <div class="container">
                <p style="color: #6c757d;">
                  <em>
                    Fields with <span style="color: red;">*</span> are required
                  </em>
                </p>
                <div class="row">
                  <div class="col-md-12">
                    <form class="needs-validation" novalidate>
                      <div class="form-row">
                        <div class="col-md-12">
                          <label for="validationCustom01" class="edit_profile_label">Old Password <span style="color: red;">*</span></label>
                          <input type="text" class="form-control edit_profile_form_control" id="validationCustom01" required>
                          <div class="valid-feedback">
                            Looks good!
                          </div>
                        </div>
                      </div>

                      <div class="form-row">
                        <div class="col-md-12">
                          <label for="validationCustom01" class="edit_profile_label">Password <span style="color: red;">*</span></label>
                          <input type="text" class="form-control edit_profile_form_control" id="validationCustom01" required>
                          <div class="valid-feedback">
                            Looks good!
                          </div>
                        </div>
                      </div>

                      <div class="form-row">
                        <div class="col-md-12">
                          <label for="validationCustom01" class="edit_profile_label">Retype Password <span style="color: red;">*</span></label>
                          <input type="text" class="form-control edit_profile_form_control" id="validationCustom01" required>
                          <div class="valid-feedback">
                            Looks good!
                          </div>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              <button type="button" class="btn btn-success">Save changes</button>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
      <i class="fas fa-angle-up"></i>
    </a>

    <?php include("js.php"); ?>
  </body>
</html>
