<!DOCTYPE html>
  <html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Buy - Livestock247</title>

    <!-- Custom fonts for this template-->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link href="../fonts/ubuntu.css" rel="stylesheet" type="text/css">

    <!-- Page level plugin CSS-->
    <link href="vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="css/sb-admin.css" rel="stylesheet">
    <link href="css/index.css" rel="stylesheet">
  </head>

  <body id="page-top">
    <!-- Header -->
    <?php include("header.php"); ?>

    <div id="wrapper">
      <!-- Sidebar -->
      <?php include("sidebar.php"); ?>

      <div id="content-wrapper">
        <div class="container-fluid">
          <!-- Breadcrumbs-->
          <ol class="breadcrumb">
            <li class="breadcrumb-item">
              <h4 class="overview_summary">Buy</h4>
            </li>
          </ol>

          <div class="jumbotron jumbotron_background">
            <h1 class="display-4">
              You are one step closer to buying your lifestock
            </h1>
            <p class="display-4_p">Fill in the required information</p>

            <div class="container">
              <div class="row">
                <div class="col-md-2"></div>
                <div class="col-md-2"></div>
                <div class="col-md-2"></div>
                <div class="col-md-2"></div>
                <div class="col-md-2">
                  <div class="full_width free_space">
                    <a href="buytour.php">
                      <div class="tooltip_hover">
                        <img src="../images/_.png" class="overvview_warning_button">
                        <span class="tooltiptext">Quick Tour Livestock</span>
                      </div>
                    </a>
                    <div class="clearfix"></div>
                  </div>
                </div>
                <div class="col-md-2"></div>
                <div class="clearfix"></div>
              </div>
            </div>

            <div class="container">
              <div class="row">
                <div class="col-md-2">
                  <select class="selectpicker">
                    <option>Type</option>
                    <option>Goat</option>
                    <option>Cow</option>
                    <option>Pig</option>
                  </select>
                </div>
                <div class="col-md-2">
                  <select class="selectpicker">
                    <option>Quantity</option>
                    <option>1</option>
                    <option>2</option>
                    <option>3</option>
                  </select>
                </div>
                <div class="col-md-2">
                  <select class="selectpicker">
                    <option>Sex</option>
                    <option>Male</option>
                    <option>Female</option>
                  </select>
                </div>
                <div class="col-md-2">
                  <select class="selectpicker">
                    <option>Weight</option>
                    <option>4kg</option>
                    <option>5kg</option>
                    <option>6kg</option>
                  </select>
                </div>
                <div class="col-md-2">
                  <select class="selectpicker">
                    <option>Breed</option>
                    <option>White Bororo</option>
                    <option>White Bororo</option>
                    <option>White Bororo</option>
                  </select>
                </div>
                <div class="col-md-2">
                  <div class="background_for_circle">
                    <img src="../images/theplus.png" class="overvview_warning_button">
                  </div>
                </div>

                <div class="full_width casual_text_center casual_margin_top">
                  <a href="buy_delivery.php" class="btn btn-success edit_profile_buy_button">
                    Continue
                  </a>
                </div>
              </div>
            </div>
          </div>

          <!-- Footer -->
          <?php include("copyright.php"); ?>
        </div>
      </div>
    </div>
    <!-- /#wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
      <i class="fas fa-angle-up"></i>
    </a>

    <?php include("js.php"); ?>
  </body>
</html>
