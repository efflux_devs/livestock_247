<ul class="sidebar navbar-nav">
  <li class="nav-item active">
    <a class="nav-link" href="index.php">
      <img src="../images/Shape.png">
      <span>Quickview</span>
    </a>
  </li>

  <li class="nav-item dropdown">
    <a class="nav-link" href="buy.php">
      <i class="fas fa-fw fa-folder"></i>
      <span>Buy</span>
    </a>
  </li>

  <li class="nav-item dropdown">
    <a class="nav-link" href="invoice.php">
      <i class="fas fa-fw fa-folder"></i>
      <span>Invoice</span>
    </a>
  </li>

  <li class="nav-item">
    <a class="nav-link" href="payment.php">
      <i class="fas fa-fw fa-chart-area"></i>
      <span>Payments</span></a>
  </li>

  <li class="nav-item">
    <a class="nav-link" href="livestock.php">
      <img src="../images/Vectors.png">
      <span>Livestock</span>
    </a>
  </li>
</ul>