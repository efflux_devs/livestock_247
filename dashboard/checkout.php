<!DOCTYPE html>
  <html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Checkout - Livestock247</title>

    <!-- Custom fonts for this template-->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link href="../fonts/ubuntu.css" rel="stylesheet" type="text/css">

    <!-- Page level plugin CSS-->
    <link href="vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="css/sb-admin.css" rel="stylesheet">
    <link href="css/index.css" rel="stylesheet">
  </head>

  <body id="page-top">
    <!-- Header -->
    <?php include("header.php"); ?>

    <div id="wrapper">
      <!-- Sidebar -->
      <?php include("sidebar.php"); ?>

      <div id="content-wrapper">
        <div class="container-fluid">
          <!-- Breadcrumbs-->
          <ol class="breadcrumb">
            <li class="breadcrumb-item">
              <h4 class="overview_summary">Buy</h4>
            </li>
          </ol>

          <div class="jumbotron invoice_summary_jumbotron_background">
            <h1 class="display-4">
              Product Added to Basket
            </h1>
            <p class="display-4_p">Order has been placed you now have to proceed payment</p>

            <div class="invoice_summary_jumbotron_background_casual_background">
              <div class="buy_details_summary_details">
                <div class="jumbotron_background_width_margin_image_parent">
                  <img src="../images/Bitmap.png" class="buy_details_jumbotron_background_width_margin_image">
                </div>

                <h1 class="display-4 display_4">
                  Hello, Bosun Jones
                </h1>

                <p class="jumbotron_background_width_margin_p">
                  Your order <b>White Bororo Cow</b> has been added to your basket to proceed for checkout
                </p>

                <hr/ style="margin: 20px 0px;">

                <h5 class="display_4 casual_margin_left">
                  <b>Product Details</b>
                </h5>

                <div class="container">
                  <div class="row">
                    <div class="col-md-4">
                      <div class="buy_details_padding">
                        <ul class="buy_details_ul">
                          <li class="buy_details_li">Type: Cow</li>
                          <li class="buy_details_li">Quantity: 20</li>
                          <li class="buy_details_li">Sex: Male</li>
                          <li class="buy_details_li">Weight: 2kg</li>
                          <li class="buy_details_li">Breed: White Cow</li>
                        </ul>
                      </div>
                    </div>
                    <div class="col-md-4 buy_details_border">
                      <div class="buy_details_padding">
                        <h5 class="buy_details_padding_h5">Product Amount</h5>
                        <p class="buy_details_padding_p">200,000.00</p>

                        <div style="margin: 15px 0px;"></div>

                        <h5 class="buy_details_padding_h5">Delivery Amount</h5>
                        <p class="buy_details_padding_p">2,000.00</p>
                      </div>
                    </div>
                    <div class="col-md-4 buy_details_border">
                      <div class="buy_details_padding full_width">
                        <h5 class="buy_details_padding_h5">Total Amount</h5>
                        <p class="buy_details_padding_p buy_details_padding_p_color">
                          <b>202,000.00</b>
                        </p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

                
              <div class="casual_margin_top full_width casual_text_center">
                <a href="make_payment.php" class="btn btn-success edit_profile_buy_button">
                  Proceed Payment
                </a>
              </div>
            </div>
          </div>

          <!-- Footer -->
          <?php include("copyright.php"); ?>
        </div>
      </div>
    </div>
    <!-- /#wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
      <i class="fas fa-angle-up"></i>
    </a>

    <?php include("js.php"); ?>
  </body>
</html>
