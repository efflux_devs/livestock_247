<!DOCTYPE html>
  <html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Tour - Livestock247</title>

    <!-- Custom fonts for this template-->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link href="../fonts/ubuntu.css" rel="stylesheet" type="text/css">

    <!-- Page level plugin CSS-->
    <link href="vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="css/sb-admin.css" rel="stylesheet">
    <link href="css/index.css" rel="stylesheet">
  </head>

  <body id="page-top">
    <!-- Header -->
    <?php include("header.php"); ?>

    <div id="wrapper">
      <!-- Sidebar -->
      <?php include("sidebar.php"); ?>

      <div id="content-wrapper">
        <div class="container-fluid">
          <!-- Breadcrumbs-->
          <ol class="breadcrumb">
            <li class="breadcrumb-item">
              <h4 class="overview_summary">Buy</h4>
            </li>
          </ol>

          <div class="jumbotron jumbotron_background">
            <h1 class="display-4">
              View the 3rd representation of the product before you make payment
            </h1>

            <div style="margin-top: 50px;">
              <div class="container">
                <div class="row">
                  <div class="col-md-9">
                    <div style="background-image: url('../images/malu.png');" class="casual_image_style"></div>
                  </div>
                  <div class="col-md-3">
                    <h3 class="buy_tour_h3">
                      <em>
                        Hints for better experience
                      </em>
                    </h3>
                    <p class="buy_tour_p">
                      <em>
                        Pan mouse around to view product in 360 degree <br><br>

                        Scroll mouse wheel in to zoom into product and do the inverse to zoom out of product<br><br>

                        Shortcut "z" is to zoom-in, "shift+z" is to zoom-out<br><br>

                        Pan mouse around to view product in 360 degree<br><br>

                        Scroll mouse wheel in to zoom into product and do the inverse to zoom out of product.
                      </em>
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <!-- Footer -->
          <?php include("copyright.php"); ?>
        </div>
      </div>
    </div>
    <!-- /#wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
      <i class="fas fa-angle-up"></i>
    </a>

    <?php include("js.php"); ?>
  </body>
</html>
