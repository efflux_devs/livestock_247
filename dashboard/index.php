<!DOCTYPE html>
  <html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Livestock247 - Overview</title>

    <!-- Custom fonts for this template-->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link href="../fonts/ubuntu.css" rel="stylesheet" type="text/css">

    <!-- Page level plugin CSS-->
    <link href="vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="css/sb-admin.css" rel="stylesheet">
    <link href="css/index.css" rel="stylesheet">
  </head>

  <body id="page-top">
    <!-- Header -->
    <?php include("header.php"); ?>

    <div id="wrapper">
      <!-- Sidebar -->
      <?php include("sidebar.php"); ?>

      <div id="content-wrapper">
        <div class="container-fluid">
          <!-- Breadcrumbs-->
          <ol class="breadcrumb">
            <li class="breadcrumb-item">
              <h4 class="overview_summary">Quicklink</h4>
            </li>
          </ol>

          <!-- Icon Cards-->
          <div class="row">
            <div class="col-xl-3 col-sm-6 mb-3">
              <div class="card text-white bg-white o-hidden h-100">
                <div class="card-body">
                  <img src="../images/shopping.svg" class="overview_icon">
                  <div class="mr-5">Purchase Orders</div>
                </div>
              </div>
            </div>

            <div class="col-xl-3 col-sm-6 mb-3">
              <div class="card text-white bg-white o-hidden h-100">
                <div class="card-body">
                  <img src="../images/point-of-service.svg" class="overview_icon">
                  <div class="mr-5">Invoices</div>
                </div>
              </div>
            </div>

            <div class="col-xl-3 col-sm-6 mb-3">
              <div class="card text-white bg-white o-hidden h-100">
                <div class="card-body">
                  <img src="../images/debit-card.svg" class="overview_icon">
                  <div class="mr-5">Payments</div>
                </div>
              </div>
            </div>

            <div class="col-xl-3 col-sm-6 mb-3">
              <div class="card text-white bg-white o-hidden h-100">
                <div class="card-body">
                  <img src="../images/book.svg" class="overview_icon">
                  <div class="mr-5">Livestock</div>
                </div>
              </div>
            </div>
          </div>

          <!-- Breadcrumbs-->
          <ol class="breadcrumb">
            <li class="breadcrumb-item">
              <h4 class="overview_summary">Buy</h4>
            </li>
          </ol>

          <div class="jumbotron jumbotron_background">
            <h1 class="display-4">
              You are one step closer to buying your lifestock
            </h1>
            <p class="display-4_p">Fill in the required information</p>

            <div class="container">
              <div class="row">
                <div class="col-md-2"></div>
                <div class="col-md-2"></div>
                <div class="col-md-2"></div>
                <div class="col-md-2"></div>
                <div class="col-md-2">
                  <div class="free_space full_width">
                    <a href="buytour.php">
                      <div class="tooltip_hover">
                        <img src="../images/_.png" class="overvview_warning_button">
                        <span class="tooltiptext">Quick Tour Livestock</span>
                      </div>
                    </a>
                    <div class="clearfix"></div>
                  </div>
                </div>
                <div class="col-md-2"></div>
                <div class="clearfix"></div>
              </div>
            </div>

            <div class="container">
              <div class="row">
                <div class="col-md-2">
                  <select class="selectpicker">
                    <option>Type</option>
                    <option>Goat</option>
                    <option>Cow</option>
                    <option>Pig</option>
                  </select>
                </div>
                <div class="col-md-2">
                  <select class="selectpicker">
                    <option>Quantity</option>
                    <option>1</option>
                    <option>2</option>
                    <option>3</option>
                  </select>
                </div>
                <div class="col-md-2">
                  <select class="selectpicker">
                    <option>Sex</option>
                    <option>Male</option>
                    <option>Female</option>
                  </select>
                </div>
                <div class="col-md-2">
                  <select class="selectpicker">
                    <option>Weight</option>
                    <option>4kg</option>
                    <option>5kg</option>
                    <option>6kg</option>
                  </select>
                </div>
                <div class="col-md-2">
                  <select class="selectpicker">
                    <option>Breed</option>
                    <option>White Bororo</option>
                    <option>White Bororo</option>
                    <option>White Bororo</option>
                  </select>
                </div>
                <div class="col-md-2">
                  <div class="background_for_circle">
                    <img src="../images/theplus.png" class="overvview_warning_button">
                  </div>
                </div>

                <div class="full_width casual_text_center casual_margin_top">
                  <a href="buy_delivery.php" class="btn btn-success edit_profile_buy_button">
                    Continue
                  </a>
                </div>
              </div>
            </div>
          </div>

          <div class="container">
            <div class="row">
              <div class="col-md-6">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item">
                    <h4 class="overview_summary">Invoice Summary</h4>
                  </li>
                </ol>
                <div class="jumbotron invoice_summary_jumbotron_background">
                  <div class="">
                    <table width="100%">
                      <tbody>
                        <tr>
                          <th class="invoice_summary_th">ID:1234567890</th>
                          <th class="invoice_content_to_right invoice_summary_th">4/2/2019</th>
                        </tr>
                      </tbody>
                    </table>

                    <table class="table table-bordered" width="100%" cellspacing="0">
                      <thead>
                        <tr>
                          <th>Type</th>
                          <th>Sex</th>
                          <th>Breed</th>
                          <th>Quantity</th>
                          <th>Weight</th>
                        </tr>
                      </thead>
                      
                      <tbody>
                        <tr>
                          <td>Cow</td>
                          <td>Male</td>
                          <td>White Bororo</td>
                          <td>400</td>
                          <td>14000kg</td>
                        </tr>
                      </tbody>
                    </table>

                    <div class="invoice_summary_details">
                      <h5 class="invoice_summary_details_h5">Payment Information</h5>
                      <p class="invoice_summary_details_p">ID:1234567890</p>

                      <ul class="invoice_summary_ul">
                        <li class="invoice_summary_li">Bank Name:</li>
                        <li class="invoice_summary_li invoice_summary_li_sec">First Bank</li>
                      </ul>

                      <ul class="invoice_summary_ul">
                        <li class="invoice_summary_li">Account Number:</li>
                        <li class="invoice_summary_li invoice_summary_li_sec">6004568392</li>
                      </ul>

                      <ul class="invoice_summary_ul">
                        <li class="invoice_summary_li">Account Name:</li>
                        <li class="invoice_summary_li invoice_summary_li_sec">Olubodun Akinleye</li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>

              <div class="col-md-6">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item">
                    <h4 class="overview_summary">Payment Summary</h4>
                  </li>
                </ol>
                <div class="jumbotron payment_summary_jumbotron">
                  <table class="table table-bordered" width="100%" cellspacing="0">
                    <thead>
                      <tr>
                        <th>Date</th>
                        <th>Description</th>
                        <th>Credit</th>
                        <th>Debit</th>
                        <th>Longtime</th>
                      </tr>
                    </thead>
                    
                    <tbody>
                      <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>

           <!-- Breadcrumbs-->
          <ol class="breadcrumb">
            <li class="breadcrumb-item">
              <h4 class="overview_summary">Statistics</h4>
            </li>
          </ol>

          <!-- Area Chart Example-->
          <div class="card mb-3">
            <div class="card-header">
              <i class="fas fa-chart-area"></i>
              Payment</div>
            <div class="card-body">
              <canvas id="myAreaChart" width="100%" height="30"></canvas>
            </div>
            <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
          </div>
        </div>
        <!-- /.container-fluid -->

        <!-- Footer -->
        <?php include("copyright.php"); ?>
      </div>
      <!-- /.content-wrapper -->
    </div>
    <!-- /#wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
      <i class="fas fa-angle-up"></i>
    </a>

    <?php include("js.php"); ?>
  </body>
</html>
