<!DOCTYPE html>
  <html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Profile - Livestock247</title>

    <!-- Custom fonts for this template-->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link href="../fonts/ubuntu.css" rel="stylesheet" type="text/css">

    <!-- Page level plugin CSS-->
    <link href="vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="css/sb-admin.css" rel="stylesheet">
    <link href="css/index.css" rel="stylesheet">
  </head>

  <body id="page-top">
    <!-- Header -->
    <?php include("header.php"); ?>

    <div id="wrapper">
      <!-- Sidebar -->
      <?php include("sidebar.php"); ?>

      <div id="content-wrapper">
        <div class="container-fluid">
          <!-- Breadcrumbs-->
          <ol class="breadcrumb">
            <li class="breadcrumb-item">
              <h4 class="overview_summary">
                My Profile
              </h4>
            </li>
          </ol>

          <div class="jumbotron jumbotron_background jumbotron_background_width_margin">
            <div class="jumbotron_background_width_margin_image_parent">
              <img src="../images/Bitmap.png" class="jumbotron_background_width_margin_image">
            </div>

            <h1 class="display-4">
              Olubodun Akinyele
            </h1>

            <p class="jumbotron_background_width_margin_p">
              akinyeleolubodun@yahoo.com
            </p>

            <p class="jumbotron_background_width_margin_p">
              1, solaru Street Off Akode Lane Ikeja Lagos
            </p>

            <p class="jumbotron_background_width_margin_p">
              Lagos | Status: Online
            </p>

            <div style="width: 100%; text-align: center; margin-top: 20px;">
              <a href="edit_profile.php" style="width: 20%; color: #fff;" type="button" class="btn btn-success">Edit Profile</a>
            </div>

            <hr/ style="border: 1px solid; margin: 20px 0px;">

            <div class="container">
              <div class="row">
                <div class="col-md-6">
                  <table width="100%">
                    <tr>
                      <td class="table_border_td">Login ID</td>
                    </tr>

                    <tr>
                      <td class="table_border_td">Phone</td>
                    </tr>

                    <tr>
                      <td class="table_border_td">Registration Date</td>
                    </tr>

                    <tr>
                      <td class="table_border_td">Last Visit</td>
                    </tr>
                  </table>
                </div>
                <div class="col-md-6">
                  <div class="table_border_left">
                    <table>
                      <tr>
                        <td>324-6491-0260</td>
                      </tr>

                      <tr>
                        <td>08068869417</td>
                      </tr>

                      <tr>
                        <td>2018-12-12 06:19:42</td>
                      </tr>

                      <tr>
                        <td>2019-02-14 19:37:09</td>
                      </tr>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /.container-fluid -->

        <?php include("copyright.php"); ?>
      </div>
      <!-- /.content-wrapper -->
    </div>

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
      <i class="fas fa-angle-up"></i>
    </a>

    <?php include("js.php"); ?>
  </body>
</html>
