<!DOCTYPE html>
  <html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Buy - Livestock247</title>

    <!-- Custom fonts for this template-->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link href="../fonts/ubuntu.css" rel="stylesheet" type="text/css">

    <!-- Page level plugin CSS-->
    <link href="vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="css/sb-admin.css" rel="stylesheet">
    <link href="css/index.css" rel="stylesheet">
  </head>

  <body id="page-top">
    <!-- Header -->
    <?php include("header.php"); ?>

    <div id="wrapper">
      <!-- Sidebar -->
      <?php include("sidebar.php"); ?>

      <div id="content-wrapper">
        <div class="container-fluid">
          <!-- Breadcrumbs-->
          <ol class="breadcrumb">
            <li class="breadcrumb-item">
              <h4 class="overview_summary">Buy</h4>
            </li>
          </ol>

          <div class="jumbotron jumbotron_background jumbotron_half_background">
            <h1 class="display-4">
              You are one step closer to buying your lifestock
            </h1>
            <p class="display-4_p">Fill in the required information</p>

            <div class="container">
              <div class="row">
                <div class="col-md-12">
                  <form class="needs-validation" novalidate>
                    <div class="form-row">
                      <div class="col-md-12">
                        <label for="validationCustom01" class="edit_profile_label">Delivery Location</label>
                        <input type="text" class="form-control edit_profile_form_control" id="validationCustom01" placeholder="Delivery Location"
                          required>
                      </div>
                    </div>

                    <div class="form-row">
                      <div class="col-md-12">
                        <label for="validationCustom01" class="edit_profile_label">Delivery Period</label>
                        <input type="text" class="form-control edit_profile_form_control" id="validationCustom01" placeholder="Delivery Period"
                          required>
                      </div>
                    </div>

                    <div class="form-row">
                      <div class="col-md-12">
                        <label for="validationCustom01" class="edit_profile_label">Delivery Mode</label>
                        <input type="text" class="form-control edit_profile_form_control" id="validationCustom01" placeholder="Delivery Mode"
                          required>
                      </div>
                    </div>

                    <div class="full_width casual_text_center">
                      <a href="checkout.php" class="btn btn-success edit_profile_buy_button">
                        Continue
                      </a>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>

          <!-- Footer -->
          <?php include("copyright.php"); ?>
        </div>
      </div>
    </div>
    <!-- /#wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
      <i class="fas fa-angle-up"></i>
    </a>

    <?php include("js.php"); ?>
  </body>
</html>
