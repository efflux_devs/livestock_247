<!DOCTYPE html>
  <html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Livestock - Livestock247</title>

    <!-- Custom fonts for this template-->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link href="../fonts/ubuntu.css" rel="stylesheet" type="text/css">

    <!-- Page level plugin CSS-->
    <link href="vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="css/sb-admin.css" rel="stylesheet">
    <link href="css/index.css" rel="stylesheet">
  </head>

  <body id="page-top">
    <!-- Header -->
    <?php include("header.php"); ?>

    <div id="wrapper">
      <!-- Sidebar -->
      <?php include("sidebar.php"); ?>

      <div id="content-wrapper">
        <div class="container-fluid">
          <!-- Breadcrumbs-->
          <ol class="breadcrumb">
            <li class="breadcrumb-item">
              <h4 class="overview_summary">Livestock</h4>
            </li>
          </ol>

          <div class="jumbotron jumbotron_background">
            <div class="container">
              <div class="row">
                <div class="col-md-4">
                  <label class="livestock_label">Purchase No.</label>
                  <div class="input-group">
                    <input type="text" class="form-control livestock_form_control">

                    <div class="input-group-append">
                      <span class="input-group-text" id="basic-addon2">
                        <img src="../images/cow-1.png" style="height: 20px;">
                      </span>
                    </div>
                  </div>
                </div>

                <div class="col-md-4">
                  <label class="livestock_label">Chip No.</label>
                  <div class="input-group">
                    <input type="text" class="form-control livestock_form_control">

                    <div class="input-group-append">
                      <span class="input-group-text" id="basic-addon2">
                        <img src="../images/cow-1.png" style="height: 20px;">
                      </span>
                    </div>
                  </div>
                </div>

                <div class="col-md-4">
                  <label class="livestock_label">Pole ID</label>
                  <div class="input-group">
                    <input type="text" class="form-control livestock_form_control">

                    <div class="input-group-append">
                      <span class="input-group-text" id="basic-addon2">
                        <img src="../images/cow-1.png" style="height: 20px;">
                      </span>
                    </div>
                  </div>
                </div>
              </div>

              <div class="card-footer small text-muted" style="margin-top: 30px;">
                <em>
                  No Results Found
                </em>
              </div>
            </div>
          </div>
        <!-- /.container-fluid -->

        <!-- Footer -->
        <?php include("copyright.php"); ?>
      </div>
      <!-- /.content-wrapper -->
    </div>
    <!-- /#wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
      <i class="fas fa-angle-up"></i>
    </a>

    <?php include("js.php"); ?>
  </body>
</html>
