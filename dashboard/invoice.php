<!DOCTYPE html>
  <html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Invoice - Livestock247</title>

    <!-- Custom fonts for this template-->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link href="../fonts/ubuntu.css" rel="stylesheet" type="text/css">

    <!-- Page level plugin CSS-->
    <link href="vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="css/sb-admin.css" rel="stylesheet">
    <link href="css/index.css" rel="stylesheet">
  </head>

  <body id="page-top">
    <!-- Header -->
    <?php include("header.php"); ?>

    <div id="wrapper">
      <!-- Sidebar -->
      <?php include("sidebar.php"); ?>

      <div id="content-wrapper">
        <div class="container-fluid">
          <!-- Breadcrumbs-->
          <ol class="breadcrumb">
            <li class="breadcrumb-item">
              <h4 class="overview_summary">Invoice</h4>
            </li>
          </ol>

          <div class="jumbotron invoice_summary_jumbotron_background">
            <div class="">
              <table width="100%">
                <tbody>
                  <tr>
                    <th class="invoice_summary_th">ID:1234567890</th>
                    <th class="invoice_content_to_right invoice_summary_th">14/2/2019</th>
                  </tr>
                </tbody>
              </table>

              <h2 class="invoice_summary_details_h2">Product Details</h2>

              <table style="text-align: center;" class="table table-bordered" width="100%" cellspacing="0">
                <thead>
                  <tr>
                    <th>Type</th>
                    <th>Sex</th>
                    <th>Breed</th>
                    <th>Quantity</th>
                    <th>Weight</th>
                    <th>Product Amount</th>
                    <th>Delivery Amount</th>
                    <th>Total Amount</th>
                  </tr>
                </thead>
                
                <tbody>
                  <tr>
                    <td>Cow</td>
                    <td>Male</td>
                    <td>White Bororo</td>
                    <td>400</td>
                    <td>14000kg</td>
                    <td>100,000</td>
                    <td>200,000</td>
                    <td>2,000,000,000</td>
                  </tr>
                </tbody>
              </table>

              <div class="invoice_summary_details">
                <h5 class="invoice_summary_details_h5">Payment Information</h5>
                <p class="invoice_summary_details_p">ID:1234567890</p>

                <ul class="invoice_summary_ul">
                  <li class="invoice_summary_li">Bank Name:</li>
                  <li class="invoice_summary_li invoice_summary_li_sec">First Bank</li>
                </ul>

                <ul class="invoice_summary_ul">
                  <li class="invoice_summary_li">Account Number:</li>
                  <li class="invoice_summary_li invoice_summary_li_sec">6004568392</li>
                </ul>

                <ul class="invoice_summary_ul">
                  <li class="invoice_summary_li">Account Name:</li>
                  <li class="invoice_summary_li invoice_summary_li_sec">Olubodun Akinleye</li>
                </ul>
              </div>

              <div class="upload_summary_details invoice_summary_details">
                <div id="drop-area">
                  <form class="my-form">
                    <div class="invoice_button_parent">
                      <input type="file" id="fileElem" multiple accept="image/*" onchange="handleFiles(this.files)">
                      <label class="button" for="fileElem">
                        <img src="../images/theplus.png">
                      </label>
                    </div>
                  </form>
                </div>
                <p class="casual_p_size">
                  <em>
                    Drag and drop your image inside the container to upload.
                  </em>
                </p>
              </div>

              <div class="invoice_summary_details upload_summary_details">
                <textarea class="invoice_summary_details_textarea" placeholder="Write a comment"></textarea>

                <p class="casual_p_size">
                  <em>
                    Write comment
                  </em>
                </p>

                <div class="full_width casual_text_center">
                  <button style="width: 20%;" type="button" class="btn btn-success">Submit</button>
                </div>
              </div>

              <div class="invoice_summary_details upload_summary_details">
                <div class="container">
                  <div class="row">
                    <div class="col-md-4">
                      <form>
                        <div class="form-group">
                          <label for="formGroupExampleInput" class="invoice_summary_details_label">Chip Number</label>
                          <input type="text" class="form-control invoice_summary_details_input" id="formGroupExampleInput" placeholder="Input Number">
                        </div>
                      </form>
                    </div>
                    <div class="col-md-4">
                      <form>
                        <div class="form-group">
                          <label for="formGroupExampleInput" class="invoice_summary_details_label">Pole ID</label>
                          <input type="text" class="form-control invoice_summary_details_input" id="formGroupExampleInput" placeholder="Input Number">
                        </div>
                      </form>
                    </div>
                    <div class="col-md-4">
                      <div class="invoice_summary_details_add_button">
                        <div class="invoice_summary_details_plus_img">
                          <img src="../images/+.png" style="height: 20px;">
                        </div>
                      </div>
                    </div>
                  </div>
                  <div clss="full_width casual_text_center">
                    <button style="width: 20%;" type="button" class="btn btn-success">Submit</button>
                  </div>
                </div>
              </div>

              <div class="invoice_summary_checkout">
                <div class="checkbox">
                  <label class="invoice_summary_checkout_label">
                    <input type="checkbox" value="" class="invoice_summary_checkout_input">
                    I confirm that this livestock is fit slaughter
                  </label>
                </div>
              </div>

              <div clss="full_width casual_text_center">
                <button style="width: 20%;" type="button" class="btn btn-success">Continue</button>
              </div>
            </div>
          </div>

          <!-- Footer -->
          <?php include("copyright.php"); ?>
        </div>
      </div>
    </div>
    <!-- /#wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
      <i class="fas fa-angle-up"></i>
    </a>

    <?php include("js.php"); ?>
  </body>
</html>
