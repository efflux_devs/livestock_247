<?php
/**
 * Created by PhpStorm.
 * User: ls247-02
 * Date: 2/12/19
 * Time: 1:20 PM
 */?>

<!DOCTYPE html>
<html lang="en">

    <meta charset="UTF-8">
    <meta name="description" content="Online Livestock Market">
    <meta name="keywords" content="Buy, Sell, Livestock, Cow, Sheep, Goat, Veterinary, Abattoir, Butchery">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!--Favicon --->
    <link rel="icon" type="image/png" sizes="16x16" href="favicon/favicon.png">
    <!--  CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet">
    <link rel="stylesheet" href="https://fonts.google.com/?query=ubu&selection.family=Ubuntu">
    <link rel="stylesheet" href="fonts/ubuntu.css">

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="css/bootstrap/css/bootstrap.min.css">
    <link href="css/bootstrap/css/bootstrap.css" type="text/css" rel="stylesheet" media="all">
    <link href="css/index.css" type="text/css" rel="stylesheet" media="all">
    <link rel="stylesheet" type="text/css" href="css/partner.css">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link rel="stylesheet" href="css/styles.css">
    <link rel="stylesheet" href="dashboard/css/index.css">

<body>
<!-- Return to Top -->
<a href="javascript:" id="return-to-top" title="Back to Top"><i class="icon-chevron-up"></i></a>
    <!-- Navigation -->
    <nav class="navbar fixed-top navbar-expand-lg topbar ">
        <div class="container">
            <a class="navbar-brand" href="index.php"><img src="images/logo.jpg" width="120" height="75" alt="Livestock247.com"></a>

            <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                  <i class="fa fa-bars"></i>
            </button>
            <div class="collapse navbar-collapse" id="navbarResponsive">
                <ul class="navbar-nav ml-auto">

                    <li class="nav-item">
                        <a class="nav-link" href="index.php">Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="about.php">About Us</a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownPortfolio" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Be a Partner
                        </a>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownPortfolio">
                            <a class="dropdown-item topbar-sub"  data-toggle="modal"  data-target="#ModalAgent" href="#">Be an Agent</a>
                            <a class="dropdown-item topbar-sub"  data-toggle="modal"  data-target="#ModalButchery"  href="">Butchery / Abattoir</a>

                        </div>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="blog.php">Blog</a>
                    </li>
                    <li class="nav-item">
                        <!-- Button trigger modal -->
                        <a class="nav-link btn btn-white" data-toggle="modal" data-target="#exampleModalCenter1" href="#">Buy Now</a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link btn btn-green" href="#" data-toggle="modal" data-target="#exampleModalCenter">Login</a>
                    </li>

                </ul>
            </div>
        </div>
    </nav>

<!-- Be an Agent Modal -->
<div class="modal fade" id="ModalAgent" tabindex="-1" role="dialog" aria-labelledby="ModalAgentTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered agent-modal" role="document">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>

            <div class="modal-body">
                <div class="agent-text">
                    <div class="row">

                        <div class="col-md-12">
                            <h5 class="modal-title" id="ModalAgent">Be an Agent</h5>
                            <p>Join the largest community of veterinary doctors, breed improvement
                                <br> specialists, and animal science professionals.</p>
                        </div>

                        <div class="col-md-12">
                            <img src="images/icon-5.png" class="img-center">
                            <h5 class="modal-title" id="ModalAgent">Becoming our Agent</h5>
                            <p>A Livestock247.com agent MUST be a qualified
                                veterinary professional<br>certified by  the veterinary council of Nigeria
                                (VCN) or the Nigeria institute of<br>animal science (NIAS)</p>
                        </div>

                        <div class="col-md-12">
                            <img src="images/icon-8.png" class="img-center">
                            <h5 class="modal-title" id="ModalAgent">Role of The Agent</h5>
                            <p>The Livestock247.com agent ordinarily has an existing relationship with a livestock producer,
                                rancher or merchant. And as such, he/she serves as the link between the platform and the livestock
                                owner by ensuring pre-slaughter evaluation as established by the quality assurance department of Livestock247.com</p>
                        </div>
                    </div><!-- row  --->
                </div>
            </div><!-- modal body -->
            <div class="modal-footer img-center">
                <button type="button" class="btn btn-green02" data-dismiss="modal" data-toggle="modal" href="#ModalAgent1">Proceed to Form</button>

            </div>
        </div>
    </div>
</div>
<!-- Modal  -->

<!-- Agent Form Modal -->
<div class="modal fade" id="ModalAgent1" tabindex="-1" role="dialog" aria-labelledby="ModalAgentTitle1" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>

            <div class="row">
                <div class="col-md-12 text-center">
                    <h5 class="modal-title login-modal" id="ModalAgent1">Be an Agent</h5>
                    <p>Fill the form below and our experts will get<br>in touch with you.</p>
                </div>
            </div>

            <div class="modal-body">
                <div class="form-field">
                    <form>
                        <div class="row">
                            <div class="col-sm-12">
                                <label>Fullname</label>
                                <div class="input-group ">
                                    <input type="text" class="form-control" placeholder="Fullname">
                                </div>
                            </div><!-- col-12 -->

                            <div class="col-sm-12">
                                <label>Agent</label>

                                <div class="input-group ">
                                    <input type="text" class="form-control" placeholder="Agent">
                                </div>
                            </div><!-- col-12 -->

                            <div class="col-sm-12">
                                <label>Phone</label>
                                <div class="input-group ">
                                    <input type="text" class="form-control" placeholder="Phone">
                                </div>
                            </div><!-- col-12 -->

                            <div class="col-sm-12">
                                <label>Emaill</label>
                                <div class="input-group ">
                                    <input type="email" class="form-control" placeholder="Email Address">
                                </div>
                            </div><!-- col-12 -->

                            <div class="col-sm-12">
                                <label>Contact Address</label>
                                <div class="input-group ">
                                    <input type="text" class="form-control" placeholder="Contact Adddress">
                                </div>
                            </div><!-- col-12 -->

                            <div class="col-sm-12">
                                <label>Business Location</label>
                                <div class="input-group ">
                                    <input type="text" class="form-control" placeholder="Business Location">
                                </div>
                            </div><!-- col-12 -->

                            <div class="col-sm-12">
                                <label>Town/City</label>
                                <div class="input-group ">
                                    <input type="text" class="form-control" placeholder="Town/City">
                                </div>
                            </div><!-- col-12 -->
                        </div><!-- row -->
                    </form>
                </div>

            </div><!-- modal body -->
            <div class="modal-footer img-center">
                <button type="button" class="btn btn-green02" data-dismiss="modal">Send</button>
            </div>

        </div>
    </div>
</div>
<!-- Modal  -->


<!-- Butchery Abattior Modal -->
<div class="modal fade" id="ModalButchery" tabindex="-1" role="dialog" aria-labelledby="ModalButcheryTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>

            <div class="row">
                <div class="col-md-12 text-center">
                    <h5 class="modal-title login-modal" id="ModalButchery">Butchery/Abattoir</h5>
                    <p>Join the largest community of Butchery/Abattoir</p>
                </div>
            </div>

            <div class="modal-body">
            </div><!-- modal body -->
            <div class="modal-footer img-center">
                <button type="button" class="btn btn-green02" data-dismiss="modal" data-toggle="modal" href="#ModalButchery1">Proceed to Form</button>
            </div>

        </div><!-- Modal content  -->
    </div>
</div>

<!-- Modal  -->

<!-- Butchery/Abattoir Form Modal -->
<div class="modal fade" id="ModalButchery1" tabindex="-1" role="dialog" aria-labelledby="ModalButcheryTitle1" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>

            <div class="row">
                <div class="col-md-12 text-center">
                    <h5 class="modal-title login-modal" id="ModalButchery1">Butchery/Abattoir</h5>
                    <p>Fill the form below and our experts will get<br>in touch with you.</p>
                </div>
            </div>

            <div class="modal-body">
                <div class="form-field">
                    <form>
                        <div class="row">
                            <div class="col-sm-12">
                                <label>Fullname</label>
                                <div class="input-group ">
                                    <input type="text" class="form-control" placeholder="Fullname">
                                </div>
                            </div><!-- col-12 -->

                            <div class="col-sm-12">
                                <label>Agent</label>

                                <div class="input-group ">
                                    <input type="text" class="form-control" placeholder="Butchery">
                                </div>
                            </div><!-- col-12 -->

                            <div class="col-sm-12">
                                <label>Phone</label>
                                <div class="input-group ">
                                    <input type="text" class="form-control" placeholder="Phone">
                                </div>
                            </div><!-- col-12 -->

                            <div class="col-sm-12">
                                <label>Emaill</label>
                                <div class="input-group ">
                                    <input type="email" class="form-control" placeholder="Email Address">
                                </div>
                            </div><!-- col-12 -->

                            <div class="col-sm-12">
                                <label>Contact Address</label>
                                <div class="input-group ">
                                    <input type="text" class="form-control" placeholder="Contact Adddress">
                                </div>
                            </div><!-- col-12 -->

                            <div class="col-sm-12">
                                <label>Business Location</label>
                                <div class="input-group ">
                                    <input type="text" class="form-control" placeholder="Business Location">
                                </div>
                            </div><!-- col-12 -->

                            <div class="col-sm-12">
                                <label>Town/City</label>
                                <div class="input-group ">
                                    <input type="text" class="form-control" placeholder="Town/City">
                                </div>
                            </div><!-- col-12 -->
                        </div><!-- row -->
                    </form>
                </div>

            </div><!-- modal body -->
            <div class="modal-footer img-center">
                <button type="button" class="btn btn-green02" data-dismiss="modal">Send</button>
            </div>

        </div>
    </div>
</div>
<!-- Modal  -->


<!-- Buy Now Modal -->
<div class="modal fade" id="exampleModalCenter1" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle1" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered buy-modal" role="document">
        <div class="modal-content" style="width: 70%;">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>

            <div class="row">
                <div class="col-md-12 text-center">
                <h5 class="modal-title text-center" id="exampleModalCenterTitle1">You are one step closer to buying your lifestock</h5>
                    <p class="buy-sub">Fill in the required information</p>
                </div>
            </div>

            <div class="modal-body">
                <div class="container">
                  <div class="row">
                    <div class="col-md-2"></div>
                    <div class="col-md-2"></div>
                    <div class="col-md-2"></div>
                    <div class="col-md-2"></div>
                    <div class="col-md-2">
                      <div class="full_width free_space">
                        <div class="tooltip_hover modal_tooltip_hover" data-toggle="modal" data-target="#exampleModalCenterTour">
                            <img src="images/_.png" class="overvview_warning_button">
                            <span class="tooltiptext">Quick Tour Livestock</span>
                        </div>
                        <div class="clearfix"></div>
                      </div>
                    </div>
                    <div class="col-md-2"></div>
                    <div class="clearfix"></div>
                  </div>
                </div>

                <div class="container buy-row">
                  <div class="row">
                    <div class="col-md-2">
                      <select class="selectpicker modal_selectpicker">
                        <option>Type</option>
                        <option>Goat</option>
                        <option>Cow</option>
                        <option>Pig</option>
                      </select>
                    </div>
                    <div class="col-md-2">
                      <select class="selectpicker modal_selectpicker">
                        <option>Quantity</option>
                        <option>1</option>
                        <option>2</option>
                        <option>3</option>
                      </select>
                    </div>
                    <div class="col-md-2">
                      <select class="selectpicker modal_selectpicker">
                        <option>Sex</option>
                        <option>Male</option>
                        <option>Female</option>
                      </select>
                    </div>
                    <div class="col-md-2">
                      <select class="selectpicker modal_selectpicker">
                        <option>Weight</option>
                        <option>4kg</option>
                        <option>5kg</option>
                        <option>6kg</option>
                      </select>
                    </div>
                    <div class="col-md-2">
                      <select class="selectpicker modal_selectpicker">
                        <option>Breed</option>
                        <option>White Bororo</option>
                        <option>White Bororo</option>
                        <option>White Bororo</option>
                      </select>
                    </div>
                    <div class="col-md-2">
                      <div class="background_for_circle">
                        <img src="images/theplus.png" class="overvview_warning_button">
                      </div>
                    </div>
                  </div>
                </div>
            </div><!-- modal body -->

            <div class="modal-footer img-center">
                <button type="button" class="btn btn-green02" data-dismiss="modal" data-toggle="modal" href="#BuyModal">Continue</button>
            </div>
        </div>
    </div>
</div>
<!-- Modal  -->

<!-- Buy Form Modal -->
<div class="modal fade" id="BuyModal" tabindex="-1" role="dialog" aria-labelledby="BuyModalTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered agent-modal" role="document">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>

            <div class="row">
                <div class="col-md-12 text-center">
                    <h5 class="modal-title modal_title_for_delivery" id="BuyModal">You are one step closer to buying your lifestock</h5>
                    <p class="modal_title_for_delivery_p">Fill in the required information</p>
                </div>
            </div>

            <div class="modal-body">
                <div class="delivery-field">
                    <form>
                        <div class="row">
                            <div class="col-sm-12">
                                <label>Delivery Location</label>
                                <div class="input-group ">
                                    <input type="text" class="form-control" placeholder="Delivery Location">
                                </div>
                            </div><!-- col-12 -->

                            <p></p>

                            <div class="col-sm-12">
                                <label>Delivery Period</label>
                                <div class="input-group">
                                    <input type="text" class="form-control" placeholder="Delivery Period">
                                </div>
                            </div><!-- col-12 -->
                            <p></p>

                            <div class="col-sm-12">
                                <label>Delivery Mode</label>
                                <div class="input-group">
                                    <input type="text" class="form-control" placeholder="Delivery Mode">
                                </div>
                            </div><!-- col-12 -->

                        </div><!-- row -->
                    </form>
                </div>

            </div>
            <div class="modal-footer img-center">
                <button type="button" class="btn btn-green02" data-dismiss="modal" data-toggle="modal" href="#BuyModal1">Continue</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal  -->

<!-- Product added to basket  Modal -->
<div class="modal fade" id="BuyModal1" tabindex="-1" role="dialog" aria-labelledby="BuyModalTitle1" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered agent-modal" role="document">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>

            <div class="row">
                <div class="col-md-12 text-center">
                    <h5 class="modal-title text-center" id="BuyModal1">Product Added to Basket</h5>
                    <p class="">Order has been placed you now have to proceed payment</p>
                </div>
            </div>

            <div class="modal-body img-center">
                <img src="images/icon-9.png" class="img-center">
                <h6 class="text-center">Hello, Bosun Jones</h6>
                <p class="text-center">Your order White Bororo Cow has been added to<br>your basket to proceed for checkout</p>
            </div><!-- modal body -->
            <div class="modal-footer img-center">
                <button type="button" class="btn btn-green02" data-dismiss="modal" data-toggle="modal" href="#BuyModal2">Proceed Payment</button>
            </div>
        </div>
    </div>
</div>
<!-- Modal  -->

<!--Billing Details Modal -->
<div class="modal fade" id="BuyModal2" tabindex="-1" role="dialog" aria-labelledby="BuyModalTitle2" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered agent-modal" role="document">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>

            <div class="row">
                <div class="col-md-12 text-center">
                    <h5 class="modal-title text-center modal_title_for_delivery" id="BuyModal2">Fill in your billing details</h5>
                    <p class="modal_title_for_delivery_p">Order has been placed you now have to<br>proceed payment</p>
                </div>
            </div>

            <div class="modal-body">
                <div class="delivery-field">
                    <form>
                        <div class="row">
                            <div class="col-sm-12">
                                <label>Bank Name</label>
                                <div class="input-group ">
                                    <input type="text" class="form-control" placeholder="Bank Name">
                                </div>
                            </div><!-- col-12 -->

                            <p></p>

                            <div class="col-sm-12">
                                <label>Account Name</label>
                                <div class="input-group">
                                    <input type="text" class="form-control" placeholder="Account Name">
                                </div>
                            </div><!-- col-12 -->
                            <p></p>

                            <div class="col-sm-12">
                                <label>Account Number</label>
                                <div class="input-group">
                                    <input type="text" class="form-control" placeholder="Account Number">
                                </div>
                            </div><!-- col-12 -->

                        </div><!-- row -->
                    </form>
                </div>

            </div><!-- modal body -->
            <div class="modal-footer img-center">
                <button type="button" class="btn btn-green02" data-dismiss="modal">Make Payment</button>

            </div>
        </div>
    </div>
</div>
<!-- Modal  -->


<!-- 3D Cattle View Modal -->
<div class="modal fade" id="exampleModalCenter2" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle2" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered cattle-modal" role="document">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>

            <div class="row">
                <div class="col-md-12 text-center">
                    <h5 class="modal-title text-center" id="exampleModalCenterTitle2">View the 3D representation of the product<br>before you make payment</h5>
                </div>
            </div>

            <div class="modal-body">
            </div><!-- modal body -->
            <div class="modal-footer img-center">
                <button type="button" class="btn btn-green02" data-dismiss="modal" data-toggle="modal" href="#exampleModalCenter3">Continue</button>

            </div>
        </div>
    </div>
</div>
<!-- Modal  -->

<!-- Login Now Modal -->
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>

            <div class="row">
                <div class="col-md-12 text-center">
                    <img src="images/logo.jpg" width="120" height="75" alt="Livestock247.com" >
                    <h5 class="modal-title login-modal" id="exampleModalCenterTitle">Login</h5>
                    <p class="login-modal-sub">Welcome back</p>
                </div>
            </div>

            <div class="modal-body">
               <div class="form-field">
                   <form>
                       <div class="row">
                           <div class="col-sm-12">
                               <label>Name</label>
                               <div class="input-group ">
                                   <input type="text" class="form-control" placeholder="Name">
                                   <div class="input-group-prepend">
                                       <div class="input-group-text"> <i class="fa fa-user"></i></div>
                                   </div>

                               </div>
                           </div><!-- col-12 -->

                       <p></p>

                           <div class="col-sm-12">
                               <label>Password</label>
                               <div class="input-group">
                                   <input type="password" class="form-control" placeholder="Password">
                                   <div class="input-group-prepend">
                                       <div class="input-group-text"><i class="fa fa-lock"></i></div>
                                   </div>
                               </div>
                           </div><!-- col-12 -->

                       </div><!-- row -->
                   </form>
               </div>

            </div><!-- modal body -->
            <div class="modal-footer img-center">
                <button type="button" class="btn btn-green02" data-dismiss="modal">Login</button>

            </div>
            <p class="text-center"><span class="signup">Don't have an account yet?</span>
                <a  style="color:#333;" title="Create Account" data-dismiss="modal" data-toggle="modal" href="#ModalCenter"> Sign Up <i class="fa fa-arrow-right"></i></a></p>
        </div>
    </div>
</div>
<!-- Modal  -->

<!-- Signup Now Modal -->
<div class="modal fade" id="ModalCenter" tabindex="-1" role="dialog" aria-labelledby="ModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>

            <div class="row">
                <div class="col-md-12 text-center">
                    <img src="images/logo.jpg" width="120" height="75" alt="Livestock247.com" >
                    <h5 class="modal-title login-modal" id="exampleModalCenterTitle">SignUp</h5>
                    <p class="login-modal-sub">For before we proceed further</p>
                </div>
            </div>

            <div class="modal-body">
                <div class="form-field">
                    <form>
                        <div class="row">
                            <div class="col-sm-12">
                                <label>Fullname</label>
                                <div class="input-group ">
                                    <input type="text" class="form-control" placeholder="Fullname">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text"> <i class="fa fa-user"></i></div>
                                    </div>

                                </div>
                            </div><!-- col-12 -->

                            <p></p>

                            <div class="col-sm-12">
                                <label>Emaill</label>
                                <div class="input-group ">
                                    <input type="email" class="form-control" placeholder="Email Address">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text"> <i class="fa fa-envelope"></i></div>
                                    </div>
                                </div>
                            </div><!-- col-12 -->

                            <div class="col-sm-12">
                                <label>Password</label>
                                <div class="input-group">
                                    <input type="password" class="form-control" placeholder="Password">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text"><i class="fa fa-lock"></i></div>
                                    </div>
                                </div>
                            </div><!-- col-12 -->

                            <div class="col-sm-12">
                                <label>Confirm Password</label>
                                <div class="input-group">
                                    <input type="password" class="form-control" placeholder="Confirm Password">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text"><i class="fa fa-lock"></i></div>
                                    </div>
                                </div>
                            </div><!-- col-12 -->

                        </div><!-- row -->
                    </form>
                </div>

            </div><!-- modal body -->
            <div class="modal-footer img-center">
                <button type="button" class="btn btn-green02" data-dismiss="modal">SignUp</button>
            </div>

        </div>
    </div>
</div>

<!-- Modal  -->

<!-- Tour Modal Begins -->
    <div class="modal fade" id="exampleModalCenterTour" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document" style="width: 650px !important; max-width: 650px !important; height: 500px !important; max-height: 500px;">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title modal_title_for_tour" id="exampleModalLongTitle">
                View the 3rd representation of the product before you make payment
            </h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="container">
                <div class="row">
                    <div class="col-md-7">
                        <div style="background-image: url(images/malu.png);" class="casual_image_style"></div>
                    </div>
                    <div class="col-md-5">
                        <h3 class="buy_tour_h3">
                          <em>
                            Hints for better experience
                          </em>
                        </h3>
                        <p class="buy_tour_p">
                          <em>
                            Pan mouse around to view product in 360 degree <br><br>

                            Scroll mouse wheel in to zoom into product and do the inverse to zoom out of product<br><br>

                            Shortcut "z" is to zoom-in, "shift+z" is to zoom-out<br><br>

                            Pan mouse around to view product in 360 degree<br><br>

                            Scroll mouse wheel in to zoom into product and do the inverse to zoom out of product.
                          </em>
                        </p>
                    </div>
                </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary">Save changes</button>
          </div>
        </div>
      </div>
    </div>
<!-- Tour Modal End -->

</body>
</html>